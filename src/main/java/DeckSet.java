import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DeckSet {
    private List<String> spawns;
    private List<String> weapons;

    public DeckSet() {
        this.spawns = read("http://cil-ibsc.fr/_Zomb/Spawn.lst");
        this.weapons = read("http://cil-ibsc.fr/_Zomb/Weapons.lst");
    }

    public List<String> getSpawns() {
        return spawns;
    }

    public List<String> getWeapons() {
        return weapons;
    }

    private List<String> read(String fileUrl) {
        List<String> lines = new ArrayList<>();
        URL url;
        try {
            url = new URL(fileUrl);
            BufferedReader read = new BufferedReader(new InputStreamReader(url.openStream()));
            String i;
            while ((i = read.readLine()) != null) lines.add(i);
            read.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}
