import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Application extends ListenerAdapter {
    private DeckSet deckSet;

    public Application(DeckSet deckSet) {
        this.deckSet = deckSet;
    }

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        String contentRaw = event.getMessage().getContentRaw();
        if (contentRaw.toLowerCase().startsWith("z:")) {
            if (contentRaw.toLowerCase().startsWith("z:spawn")) {
                try {
                    List<String> spawns = deckSet.getSpawns();
                    int nombreAleatoire = 0 + (int) (Math.random() * ((spawns.size() - 0)));
                    System.out.println(spawns.get(nombreAleatoire));
                    URL url = new URL("http://cil-ibsc.fr/_Zomb/Spawn/" + spawns.get(nombreAleatoire));
                    BufferedImage img = ImageIO.read(url);
                    File file = new File("temp.jpg"); // change the '.jpg' to whatever extension the image has
                    ImageIO.write(img, "jpg", file); // again, change 'jpg' to the correct extension
                    event.getChannel().sendFile(file).queue();
                } catch (Exception e) {

                }
            } else if (contentRaw.toLowerCase().startsWith("z:fouille")) {
                try {
                    List<String> spawns = deckSet.getWeapons();
                    int nombreAleatoire = 0 + (int) (Math.random() * ((spawns.size() - 0)));
                    System.out.println(spawns.get(nombreAleatoire));
                    URL url = new URL("http://cil-ibsc.fr/_Zomb/Weapons/" + spawns.get(nombreAleatoire));
                    BufferedImage img = ImageIO.read(url);
                    File file = new File("temp.jpg"); // change the '.jpg' to whatever extension the image has
                    ImageIO.write(img, "jpg", file); // again, change 'jpg' to the correct extension
                    event.getChannel().sendFile(file).queue();
                } catch (Exception e) {

                }
            } else if (contentRaw.toLowerCase().startsWith("z:trouve(ak47)")) {
                try {
                    System.out.println("trouve");
                    URL url = new URL("http://zombicide.jhugues.fr/wp-content/plugins/zombicide/web/rsc/images/equipments/08025-thumb.jpg");
                    BufferedImage img = ImageIO.read(url);
                    File file = new File("temp.jpg"); // change the '.jpg' to whatever extension the image has
                    ImageIO.write(img, "jpg", file); // again, change 'jpg' to the correct extension
                    event.getChannel().sendFile(file).queue();
                } catch (Exception e) {

                }
            } else {
                event.getChannel().sendMessage("Ordre " + contentRaw + " inconnu").queue();
            }
        }
    }

}