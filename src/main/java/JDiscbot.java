import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDABuilder;

import javax.security.auth.login.LoginException;

public class JDiscbot {
    public static void main(String[] argv) throws LoginException {
        JDABuilder builder = new JDABuilder(AccountType.BOT);
        System.out.println(argv[0]);
        builder.setToken(argv[0]);
        DeckSet deckSet = new DeckSet();
        builder.addEventListeners(new Application(deckSet));
        builder.build();
    }
}
